provider "google" {
  project = "my-project-id"
  region  = "us-central1"
}

resource "google_cloud_run_service" "airplane_api" {
  name     = "airplaneAPI"
  location = "us-central1"

  template {
    spec {
      containers {
        image = "registry.gitlab.com/daniel.ascencio.hz/dascencio-latam-challenge/main:latest"
        env = [
            env { name  = "MONGO_HOST", value = "my-value" },
            env { name  = "MONGO_UERNAME", value = "my-value" },
            env { name  = "MONGO_PASSWORD", value = "my-value" },
            env { name  = "MONGO_DATABASE", value = "my-value" },
            env { name  = "MONGO_COLLECTION", value = "my-value" },
            env { name  = "JWT_AUTH_SECRET_KEY", value = "my-value" },
        ]
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}