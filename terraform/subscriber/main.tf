provider "google" {
  project = "my-project-id"
  region  = "us-central1"
}

resource "google_cloud_run_service" "subscriber" {
  name     = "subscriberDesafiolatam"
  location = "us-central1"

  template {
    spec {
      containers {
        image = "desafiolatam/subscriber:1.0.0"
        env = [
          env { name  = "MQTT_BROKER", value = "my-value" },
          env { name  = "MQTT_USERNAME", value = "my-value" },
          env { name  = "MQTT_PASSWORD", value = "my-value" },
          env { name  = "MQTT_TOPIC", value = "my-value" },
          env { name  = "MONGO_HOST", value = "my-value" },
          env { name  = "MONGO_UERNAME", value = "my-value" },
          env { name  = "MONGO_PASSWORD", value = "my-value" },
          env { name  = "MONGO_DATABASE", value = "my-value" },
          env { name  = "MONGO_COLLECTION", value = "my-value" }
        ]
      }
    }
  }
}