provider "google" {
  project = "my-project-id"
  region  = "us-central1"
}

resource "google_cloudfunctions_function" "publish_function" {
  name        = "publishDesafiolatam"
  description = "Función que publica la data desde el archivo JSON."
  runtime     = "python37"
  entry_point = "publishDesafiolatam"
  source_archive_bucket = "desafiolatam"
  source_archive_object = "functions/publish.zip"
  trigger_http = true

  environment_variables = {
    "MQTT_BROKER" = "idc11deb.emqx.cloud"
    "MQTT_USERNAME" = "desafiolatam"
    "MQTT_PASSWORD" = "desafiolatampw"
    "MQTT_TOPIC" = "desafiolatam"
  }

  depends_on = [
    google_storage_bucket_object.publish_zip
  ]
}

resource "google_storage_bucket_object" "publish_zip" {
  name   = "publish.zip"
  bucket = "desafiolatam"

  source = "functions/publish.zip"
}

resource "google_cloud_scheduler_job" "publish_scheduler" {
  name = "publishScheduler"
  schedule = "0 3 * * *"
  time_zone = "America/Santiago"
  target_type = "http"
  http_target {
    uri = google_cloudfunctions_function.publish_function.https_trigger_url
    http_method = "POST"
  }
}