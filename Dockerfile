FROM python:3.12-alpine

RUN apk update \
    && pip install --upgrade pip

WORKDIR /app

COPY . .

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 80

CMD [ "python", "main.py" ]