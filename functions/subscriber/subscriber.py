import random
import os
import pymongo
import json
from paho.mqtt import client as mqtt_client

mqtt_broker = os.environ['MQTT_BROKER']
mqtt_username = os.environ['MQTT_USERNAME']
mqtt_password = os.environ['MQTT_PASSWORD']
mqtt_topic = os.environ['MQTT_TOPIC']
mqtt_client_id = f'publish-{random.randint(0, 1000)}'
mongo_host = os.environ['MONGO_HOST']
mongo_username = os.environ['MONGO_UERNAME']
mongo_password = os.environ['MONGO_PASSWORD']
mongo_client = pymongo.MongoClient("mongodb+srv://"+mongo_username+":"+mongo_password+"@"+mongo_host+"/?retryWrites=true&w=majority")
mongo_database = mongo_client[os.environ['MONGO_DATABASE']]
mongo_collection = mongo_database[os.environ['MONGO_COLLECTION']]

def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(mqtt_client_id)
    client.username_pw_set(mqtt_username, mqtt_password)
    client.on_connect = on_connect
    client.connect(mqtt_broker, 1883)
    return client

try:
    mongo_db_names = mongo_client.list_database_names()
    print("Connected to MongoDB!")
except pymongo.errors.ConnectionFailure as e:
    print("Failed to connect, return code:", e)

def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        print('Received --> '+msg.payload.decode())
        mongo_data_msg = json.loads(msg.payload.decode())
        mongo_collection.insert_one(mongo_data_msg)

    client.subscribe(mqtt_topic)
    client.on_message = on_message

def run():
    client = connect_mqtt()
    subscribe(client)
    client.loop_forever()

if __name__ == '__main__':
    run()