import json
import os
import random
import time
from paho.mqtt import client as mqtt_client

mqtt_broker = os.environ['MQTT_BROKER']
mqtt_username = os.environ['MQTT_USERNAME']
mqtt_password = os.environ['MQTT_PASSWORD']
mqtt_topic = os.environ['MQTT_TOPIC']
mqtt_client_id = f'publish-{random.randint(0, 1000)}'

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(mqtt_client_id)
    client.username_pw_set(mqtt_username, mqtt_password)
    client.on_connect = on_connect
    client.connect(mqtt_broker, 1883)
    return client

def publish(client):
    time.sleep(1)
    with open('data.json', 'r') as json_file:
        for json_line in json_file:
            data_line = json.loads(json_line)
            result = client.publish(mqtt_topic, json.dumps(data_line))
            status = result[0]
            if status == 0:
                print("Send "+str(data_line)+" to topic "+mqtt_topic+"")
            else:
                print(f"Failed to send message to topic {mqtt_topic}")

def run():
    client = connect_mqtt()
    client.loop_start()
    publish(client)
    client.loop_stop()

if __name__ == '__main__':
    run()