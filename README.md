# Desafío Técnico DevSecOps/SRE

## Contexto

Se requiere un sistema para ingestar y almacenar datos en una DB con la finalidad de hacer analítica avanzada. Posteriormente, los datos almacenados deben ser expuestos mediante una API HTTP para que puedan ser consumidos por terceros.

## Objetivo

Desarrollar un sistema en la nube para ingestar, almacenar y exponer datos mediante el uso de IaC y despliegue con flujos CI/CD. Hacer pruebas de calidad, monitoreo y alertas para asegurar y monitorear la salud del sistema.

# Desarrollo 🚀

## Parte 1: Infraestructura e IaC. 🔧

La infraestructura necesaria para ingestar, almacenar y exponer los datos, es:

```
Google Cloud Scheduler.
Google Cloud Function.
Google Cloud Storage.
Google Cloud Run.
EMQX Service (MQTT).
Mongo Atlas.
```

### Script Functions

* **functions/publish/publish.py**: Función que lee un archivo Json llamdo data.json para posteriormente publicar su contenido en un servidor MQTT.

El archivo data.json debe alojar en la misma ruta donde se encuentra el script publish.py, de lo contrario el script no podrá ser ejecutado correctamente.

**_Mejora: El archivo json.data debe ser almacenado en un bucket de Google Cloud para que sea procesado por el publish.js. Utilinzar una cuenta de servicio con los siguientes roles y permisos sobre el Bucket_**

```
storage.objects.get
storage.objects.list
```

* **functions/subscriber/subscriber.py**: Función que se suscribe al tópico para almacenar en una base de datos mongo la data recibida desde el publicador.

### Requerimientos

Código:

```
python >= 3.7
```

Librerias:

```
paho.mqtt
pymongo
```

Seguir los siguientes pasos para instalar las librerías requeridas:

```
$ pip install --upgrade pip

$ pip install paho.mqtt pymongo

```

También se pueden instalar a través de un archivo requirements.txt

```
pip install -r requirements.txt
```

### Variables de Entorno

```
* MQTT_BROKER: Dirección URL o Host del servidor MQTT.
* MQTT_PORT: Puerto del servidor MQTT.
* MQTT_USERNAME: Usuario del servidor MQTT.
* MQTT_PASSWORD: Contraseña del servidor MQTT.
* MQTT_TOPIC: Nombre del Tópico donde serán almacenados los mensajes.
* MONGO_HOST: Dirección URL del servidor Mongo.
* MONGO_UERNAME: Usuario de conexión para la base de datos Mongo.
* MONGO_PASSWORD: Contraseña del usuario mongo.
* MONGO_DATABASE: Nombre de dase de datos Mongo.
* MONGO_COLLECTION: Nombre de la colección de base de datos Mongo donde serán almacenados los objetos json.
```

### Docker subscriber function

Para crear una imagen de docker (local) e iniciar el servicio, es necesario seguir los siguientes paos:

```
1. Ingresar a la carpeta functions -> subscriber
   cd functions/subscriber
2. Construir imagen docker
   docker build -t desafiolatam/subscriber:1.0.0 .
3. Crear un archivo llamado subscriber.env y cargar todas las variables de entorno requeridas para la ejecución de la función.
4. Ejecutar un contenedor o proceso del suscriptor
   docker run --env-file subscriber.env desafiolatam/subscriber:1.0.0
```

### Terraform

Utilizar los siguientes comandos para preparar el entorno, validar las configuraciones, planear y ejecutar los scripts de terraform:

```
$ terraform init

$ terraform fmt

$ terraform validate

$ terraform plan

$ terraform apply
```

## Parte 2: Aplicaciones y flujo CI/CD ⚙️

### API HTTP

* Descripción: Servicio HTTP que consume la data de MongoDB para exponerla bajo una API en Python >3.7 con FastAPI.
* Repositorio: https://gitlab.com/daniel.ascencio.hz/dascencio-latam-challenge
* Visivilidad: Público.
* Container Registry: https://gitlab.com/daniel.ascencio.hz/dascencio-latam-challenge/container_registry

#### Autenticación

Se debe implementar un sistema de autenticación basado en Jason Web Tokens (JWT) para administrar el acceso de los usuarios hacia la API.

Las librerias necesarias para operar con JWT son:

```
passlib
python-jose
PyJWT
```

Para implementar un sistema de autenticación basado en JWT, seguir los siguientes pasos:

1. Añadir la siguientes líneas al archivo `main.py` ubicado en la ríz del proyecto:

```
from fastapi import FastAPI, Depends, HTTPException
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from jose import jwt, JWTError
```

2. Añadir la configuración de seguridad de JWT

```
async def get_current_user(token: str = Depends(app_security)):
    try:
        payload = jwt.decode(token, app_security_key, algorithms=["HS256"])
        username: str = payload.get("sub")
        if username is None:
            raise HTTPException(status_code=401, detail="Invalid authentication credentials")
        return username
    except JWTError:
        raise HTTPException(status_code=401, detail="Invalid authentication credentials")
```

3. Proteger un endpoint

```
@app.get("/airplanes/list")
async def getAllAirplanes(current_user: str = Depends(get_current_user)):
    return listAllAirplanes
```

#### Endpoints

A continuación se listan algunos de los endpoints de la API HTTP Airplanes:

* airplanes/list
  Lista todos los registros de la base de datos.

```
curl -X GET my-domain.desafiolatam.com/airplanes/list
```

* airplanes/{airplaneCode}
  Buscar un airplane en la base de datos Mongo a partir del código del airplane el cual es recibido bajo un parámetro en la URL.

```
curl -X GET my-domain.desafiolatam.com/airplanes/{airplaneCode}
```

![Documentación API HTTP](images/api-docs.png)

Para obtener más información sobre todos los endpoints y sus parámetros, consultar la [documentación](https://my-domain.desafiolatam.com/docs).

#### Seguridad Web

Implementar los siguientes headers de seguiridad en servidor Web o en el Ingress Controller de un Clúster Kubernetes para robusteser la comunicación entre cliente y servidor:

```
Referrer-Policy: strict-origin
Strict-Transport-Security: max-age=31536000; includeSubDomains
X-Content-Type-Options: nosniff
X-XSS-Protection: 1; mode=block
```

### Flujo CICD Gitlab

Para configurar y armar un flujo de CICD se utiliza Gitlab AutoDevOps. Este cuenta con una serie de herramientas preconfiguradas que apoyan el proceso de construcción, pruebas y delivery del software.

Para integrar un repositorio Gitlab con el auto devops se deben incluir las siguientes líneas al archivo `.gitlab-ci.yml` del repositorio:

```
include:
  - template: Auto-DevOps.gitlab-ci.yml
```

Ya configurado el Template de Gitlab Auto DevOps, de manera automática estamos dentro de un flujo de CICD donde se ejecutan por defecto los siguientes JOBS:

1. Build: Proceso de construcción y registro de una imagen Docker a partir de un archivo Dockerfile ubicado en la raíz del repositorio.
Gitlab Auto DevOps detecta de manera automática el código y ejecuta los templates necesarios para construir y registrar de manera correcta las imágenes Docker en el repositorio.
2. Tests: Se ejecutan los Jobs que validan la seguridad y el correcto funcionamiento del servicio o la versión que será desplegada. Algunos de los tests a ejecutar son:

   * Code Quality.
   * Container Scanning.
   * Secret Detection.
   * Semgrep SAST.

   Por otro lado, Gitlab Auto DevOps cuenta con una serie de pruebas y validaciones de seguridad que pueden ser integradas dependiendo del plan a contratar. El el siguiente [link](https://docs.gitlab.com/ee/topics/autodevops/) se puede encontrar todo el detalle.
3. Deploy.
4. Performance.

#### Diagrama CICD

![Diagrama de CICD](images/diagrama-cicd.png)

#### Deploy Cloud Run

Para desplegar a Google Cloud Run es necesario seguir los siguientes pasos:

1. Crear una cuenta de servicio IAM y una llave de acceso Json en Google Cloud que tenga acceso al proyecto con los siguientes roles:

```
run.jobs.get
run.jobs.list
run.jobs.delete
run.jobs.create
run.jobs.createTagBinding
run.jobs.deleteTagBinding
run.executions.list
run.executions.get
run.executions.delete
run.configurations.list
run.configurations.get
```

2. Agregar las siguientes variables de Entorno al repositorio:

```
GOOGLE_APPLICATION_CREDENTIALS
GCP_PROJECT
GCP_REGION
```

3. Configurar las siguientes líneas en el archivo de CICD

`Deploy a QA`

```
deploy_cloud_run_qa:
  image: google/cloud-sdk:alpine
  stage: deploy
  environment:
    name: qa
  services:
    - docker:19.03-dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - gcloud auth activate-service-account --key-file $GOOGLE_APPLICATION_CREDENTIALS
    - gcloud config set project $GCP_PROJECT
    - gcloud auth configure-docker --quiet
  script:
    - docker image pull $DOCKER_IMAGE
    - gcloud run deploy $GCP_SERVICE --image $DOCKER_IMAGE --platform managed --region $GCP_REGION
  dependencies:
    - build
  variables:
    DOCKER_IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
    DOCKER_HOST: tcp://docker:2375
    GCP_PROJECT: $GCP_PROJECT
    GCP_REGION: $GCP_REGION
    GCP_SERVICE: $CI_PROJECT_NAME-qa
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
```

`Deploy a PROD`

```
deploy_cloud_run_qa:
  image: google/cloud-sdk:alpine
  stage: deploy
  environment:
    name: prod
  services:
    - docker:19.03-dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - gcloud auth activate-service-account --key-file $GOOGLE_APPLICATION_CREDENTIALS
    - gcloud config set project $GCP_PROJECT
    - gcloud auth configure-docker --quiet
  script:
    - docker image pull $DOCKER_IMAGE
    - gcloud run deploy $GCP_SERVICE-prod --image $DOCKER_IMAGE --platform managed --region $GCP_REGION
  dependencies:
    - build
  variables:
    DOCKER_IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
    DOCKER_HOST: tcp://docker:2375
    GCP_PROJECT: $GCP_PROJECT
    GCP_REGION: $GCP_REGION
    GCP_SERVICE: $CI_PROJECT_NAME-prod
  rules:
    - if: $CI_COMMIT_TAG =~ /^PROD-.*/
  when: manual
```

#### Deploy K8S

Para desplegar en un clúster Kubernetes existente, es necesario configura las siguientes líneas en el archivo .gitlab-ci.yml e incluir un Job para que el servicio sea desplegado en dicha infraestructura:

1. Configurar en el repositorio y/o grupo, las siguientes variables de entorno:

```
KUBE_SERVER
KUBE_TOKEN
KUBE_CA_PEM
```

2. Archivo .gitlab-ci.yml

```
include:
  - template: Auto-DevOps.gitlab-ci.yml

variables:
  AUTO_DEVOPS_BUILD_IMAGE_FORWARDED_CI_VARIABLES: "true"
  DOCKERFILE_PATH: Dockerfile
  KUBECONFIG: /root/.kube/config
  KUBE_NAMESPACE: $CI_PROJECT_NAME
  KUBE_SERVER: $KUBE_SERVER
  KUBE_TOKEN: $KUBE_TOKEN
  KUBE_CA_PEM: $KUBE_CA_PEM
```

3. Configurar nuevo JOB para desplegar en el clúster kubernetes:

```
deploy_qa_k8s:
  extends: staging
  stage: deploy
  environment:
    name: qa
  rules:
    - if: $CI_COMMIT_BRANCH == "develop"
      variables:
        HELM_RELEASE_NAME: $CI_PROJECT_NAME-qa
        HELM_EXTRA_ARGS: '--set application.namespaces=$CI_PROJECT_NAME --set readinessProbe.path="/",livenessProbe.path="/" --set service.url=$CI_PROJECT_NAME-qa.desafiolatam.com --set service.externalPort=80 --set service.internalPort=80 --set hpa.enabled=true --set hpa.minReplicas=1 --set hpa.maxReplicas=10 --set hpa.targetCPUUtilizationPercentage=60'
```

La variable `HELM_EXTRA_ARGS` es una variable que se encuentra en el HELM Template por defecto que utiliza Gitlab AutoDevOps y que permite modificar los valores por defecto para adaptar mi deploy en kubernetes y desplegarlo de manera customizada.

El Helm Template puede ser encontrado en el siguiente [link](https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image/-/tree/master/assets/auto-deploy-app).

### Diagrama de arquitectura 📋

![Diagrama de Arquitectura Desafío LATAM](images/diagrama-de-arquitectura.png)

1. **Cloud Scheduler**: Ejecuta automáticamente una función de Google.
2. **Cloud Function**: Almacena la función que será ejecutada para leer y publicar los mensajes en un tópico de MQTT Server.
3. **MQTT Server**: Utilizado para enviar y recibir mensajes en un tópico. Los clientes deben suscribirse (subscriber.py) a uno o más tópicos para recibir la información enviada desde un publisher (publisher.py).

![EMQX overview cloud](images/emqx-cloud-overview.png)

![EMQX metrics cloud](images/emqx-cloud-metrics.png)

4. **Cloud Run**: Ejecuta el suscriptor que escuchará todas las publicaciones de un tópico en específico para posteriormente escribirlas en una base de Datos Mongo.
5. **Mongo Atlas**: Servicio de Base de Datos que almacena en formato Json todas las publicaciones de los tópicos.
6. **API HTTP**: Backend que lista y/o busca un tópico en la base de datos Mongo.

## Parte 3: Pruebas de Integración y Puntos Críticos de Calidad

### Test de integración

1. Se creo un test de integración que verifica que la API esta exponiendo correctamente los datos desde la base de datos. Este test te integración utiliza el endpoint  `/airplanes/{airplaneCode}` para para buscar un Airplane en base a su código de avión, y, en caso de no ser encontrado por la base de datos, este va a retornar un error 404.

El código de integración en un pipeline de Gitlab sería el siguiente:

```
integration_api_check:
  stage: test
  image: python:3.12-alpine
  before_script:
    - apk update
    - apk add httpie
  script:
    - http --check-status my-domain.desafiolatam.com/airplanes/23331 > /dev/null && (echo "La respuesta es exitosa" && exit 0) || (echo "La respuesta no es exitosa" && exit 1)
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
    - if: $CI_COMMIT_TAG =~ /^PROD-.*/
```

2. Se crearon los siguientes endpoints para chequear la salud del servidor web de la API, los cuales responden con un código HTTP 200 si este se encuentra funcionando. Los endpoints son:

* /live
* /ready
* /health

Por otro lado, las pruebas de integración que pueden implementarse para validar que el sistema se encuentra funcionando correctamente son:

* Pruebas de Carga: Ayudan a encontrar cuellos de botella y problemas de rendimiento. Estas pruebas de integración pueden configurarse utilizando Apache JMeter en un pipeline de Gitlab o simplemente se pueden programar para su ejecución manual en un ambiente controlado.

* Pruebas de Seguridad: Evalúan la seguridad del sistema y buscan vulnerabilidades que puedan ser explotadas por los atacantes. Para implementar dichas pruebas de seguridad se pueden utilizar herramientas como OWASP ZAP o Burp Suite.
Gitlab Auto DevOps posee una serie de herramientas que pueden ser implementadas dentro de un flujo de CICD para encontrar vulnerabilidades a nivel de código y/o sistemas operativos.
En el siguiente [link](https://docs.gitlab.com/ee/topics/autodevops/) se encuentran todas las integraciones que pueden aplicarse de manera automática a un flujo de CICD.

3. Algunos puntos críticos del sistema que pueden identificarse para evitar errores o problemas de performance son:

* Tiempos de respuesta y carga de de la página.

* Tasa de Errores HTTP y/o Base de datos.

4. Para corregir los problemas críticos descritos en el punto `número 3` son:

* Tiempos de respuesta y carga de de la página: Implementar un CDN para para mejorar los tiempos de respuesta  y carga del servicio.

* Tasa de Errores HTTP y/o Base de datos: Optimizar las consultas a la base de datos e indexar las tablas.

## Parte 4: Métricas y Monitoreo

1. Además de las métricas básicas de un sistema de monitoreo (CPU, RAM y uso de Disco) existen 3 métricas más a medir para entender la salud y el rendimiento del sistema end-to-end, las cuales corresponde a:

* Tasa de Errores.
* Tiempos de respuesta.
* Latencia de red.

2. Como propuesta de herramienta de monitoreo para mejorar el rendimiento y la optimización de los recursos en la nube propondíá las siguientes herramientas:

* Dynatrace, Datadog y/o NewRelic.
  - Métricas de Rendimiento: Ayuda identificar cuellos de botella y de rendimiento de la aplicación.
  - Métricas de Infraestuctura: Ayudan a resolver problemas de uso de recursos (capacidad) para planificar de mejor manera la escalabilidad del servicio.
  - Métricas de usuario final: Ayudan a comprender como los usuario interatua con el servicio.

3. Para configurar y/o implementar Newrelic es necesario seguir los siguientes pasos:

```
1. Crear una cuenta de NewRelic y un APM para el servicio.

2. Configurar la integración de NewRelic en el Dockerfile del servicio con los siguientes comandos.

RUN curl https://download.newrelic.com/nri-bundle/nri-bundle-latest.x86_64.rpm -o nri-bundle-latest.x86_64.rpm && \
    rpm -i nri-bundle-latest.x86_64.rpm && \
    rm nri-bundle-latest.x86_64.rpm

3. Agregar las siguientes variables de entorno al archivo Dockerfile para configurar el agente de NewRelic.

ENV NRIA_LICENSE_KEY=<YOUR_LICENSE_KEY>
ENV NRIA_DISPLAY_NAME=<YOUR_DISPLAY_NAME>
ENV NRIA_INTEGRATIONS=<YOUR_INTEGRATIONS>

4. Agregar el siguiente código al Dockerfile para iniciar el agente

CMD ["/usr/bin/newrelic-infra", "-config", "/etc/newrelic-infra.yml"]

5. Iniciar el servicio y verificar que la data se esta enviando a NewRelic.
```

5. Las dificultades o límnitaciones que pueden encontrarse en un monitero deficiente son:

* Establecer límites de uso incorrectos probocando errores de throttling.
* Establecer umbrales de escalamiento (vertical/horizontal) incorrectos.
