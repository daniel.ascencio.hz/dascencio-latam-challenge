from operator import ge
import os
import pymongo
import json
from bson.json_util import dumps
from fastapi import FastAPI, Depends, HTTPException
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from jose import jwt, JWTError

app = FastAPI()
app_security = HTTPBearer()
app_security_key = os.environ['SECRET_KEY']
app_host = '0.0.0.0'
app_port = 80
mongo_host = os.environ['MONGO_HOST']
mongo_username = os.environ['MONGO_UERNAME']
mongo_password = os.environ['MONGO_PASSWORD']
mongo_client = pymongo.MongoClient("mongodb+srv://"+mongo_username+":"+mongo_password+"@"+mongo_host+"/?retryWrites=true&w=majority")
mongo_database = mongo_client[os.environ['MONGO_DATABASE']]
mongo_collection = mongo_database[os.environ['MONGO_COLLECTION']]

@app.get("/")
async def root():
    return {"message": "Bienvenido al desafío LATAM!"}

@app.get("/live")
async def root():
    return {"status": "ok"}

@app.get("/ready")
async def root():
    return {"status": "ok"}

@app.get("/health")
async def root():
    return {"status": "ok"}
 
async def get_current_user(token: str = Depends(app_security)):
    try:
        payload = jwt.decode(token, app_security_key, algorithms=["HS256"])
        username: str = payload.get("sub")
        if username is None:
            raise HTTPException(status_code=401, detail="Invalid authentication credentials")
        return username
    except JWTError:
        raise HTTPException(status_code=401, detail="Invalid authentication credentials")

def is_json_array_empty(json_array):
    if json.loads(json_array):
        return False
    else:
        return True

listAllAirplanes = dumps(list(mongo_collection.find()))
@app.get("/airplanes/list")
async def getAllAirplanes(current_user: str = Depends(get_current_user)):
    return listAllAirplanes

@app.get("/airplanes/{airplanecode}")
async def getAirplane (airplanecode: int, current_user: str = Depends(get_current_user)):
    getAirplane = dumps(list(mongo_collection.find({"airplaneCode": int(airplanecode)})))
    if not is_json_array_empty(getAirplane):
        return getAirplane
    else:
        raise HTTPException(status_code=404, detail="airplaneCode not found!")

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host=""+app_host+"", port=app_port)